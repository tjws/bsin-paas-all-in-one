package me.flyray.bsin.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 客户类型
 * 客户类型 0、个人客户 1、企业客户 2、租户(dao)客户
 */
public enum CustomerType {

    /**
     * 个人客户 Personal
     */
    PERSONAL("1", "个人客户"),

    /**
     * 企业客户
     */
    ENTERPRISE("1", "企业客户"),
    /**
     * 租户(dao)客户
     */
    TENANT_DAO("1", "租户(dao)客户");

    private String code;

    private String desc;

    CustomerType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * Json 枚举序列化
     */
    @JsonCreator
    public static CustomerType getInstanceById(Integer id) {
        if (id == null) {
            return null;
        }
        for (CustomerType status : values()) {
            if (id.equals(status.getCode())) {
                return status;
            }
        }
        return null;
    }

}
