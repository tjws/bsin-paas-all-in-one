package me.flyray.bsin.gateway.service;

import java.util.Map;

import me.flyray.bsin.gateway.domain.BsinChoreographyServiceDo;


public interface BsinChoreographyServiceService {

    BsinChoreographyServiceDo getChoreographyServiceByServiceAndMethod(Map<String, Object> requestMap);
}
