package me.flyray.bsin.gateway.service;

import com.alipay.sofa.common.utils.StringUtil;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.enums.CustomerType;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.gateway.common.ApiResult;
import me.flyray.bsin.gateway.context.BaseContextHandler;
import me.flyray.bsin.oss.ipfs.BsinIpfsService;
import me.flyray.bsin.utils.BsinServiceInvokeUtil;

@Log4j2
@Service
public class BsinIpfsRequestHandlerService {

    @Value("${bsin.s11edao.ipfs.api}")
    private String ipfsApi;
    @Value("${bsin.s11edao.ipfs.gateway}")
    private String ipfsGateway;
    @Autowired
    private BsinServiceInvokeUtil bsinServiceInvokeUtil;
    @Autowired
    private BsinIpfsService bsinIpfsService;

    public ApiResult ipfsRequestHandler(String methodName, Map<String, Object> bizParams) throws IOException {
        String currentPath = (String) bizParams.get("currentPath");
        if (StringUtil.isBlank(currentPath)) {
            currentPath = "";
        }
        if ("makeDirectory".equals(methodName)) {
            String dirName = (String) bizParams.get("dirName");
            String dirCode = (String) bizParams.get("dirCode");
            String dirDescription = (String) bizParams.get("dirDescription");
            String tenantId = (String) BaseContextHandler.get("tenantId");
            String customerNo = (String) BaseContextHandler.get("customerNo");
            String customerType = (String) BaseContextHandler.get("customerType");
            // 根据用户类型判断用户属于哪个平台
            String dev = "bigan";
            if (CustomerType.TENANT_DAO.getCode().equals(customerType)) {
                dev = "daobook";
            }
            String dirPath = "/" + dev + "/" + tenantId + currentPath + "/" + dirName;

            log.debug("mkdir: " + dirPath);
            System.out.println("mkdir: " + dirPath);
            try {
                // TODO 判断文件夹是否存在
                bsinIpfsService.mkDir("/" + dev + "/" + tenantId);
            } catch (Exception e) {
                log.error(e.toString());
                throw new BusinessException(ResponseCode.IPFS_MK_DIR_ERROR);
            }
            String fileAddress;
            try {
                fileAddress = bsinIpfsService.mkDir(dirPath);
            } catch (Exception e) {
                log.error(e.toString());
                // 文件夹名称重复
                throw new BusinessException(ResponseCode.IPFS_DIR_IS_EXISTS);
            }
            // 创建数据库数据
            Map<String, Object> requestMap = new HashMap<>();
            requestMap.put("tenantId", tenantId);
            requestMap.put("fileName", dirName);
            requestMap.put("customerNo", customerNo);
            requestMap.put("fileAddress", fileAddress);
            requestMap.put("fileCode", dirCode);
            requestMap.put("fileDescription", dirDescription);

            Map map = bsinServiceInvokeUtil.genericInvoke("AdminIpfsFileService", "makeDirectory", requestMap);

            return ApiResult.ok();
        }

        /*if ("ipfsLs".equals(methodName)) {
            String tenantId = (String) BaseContextHandler.get("tenantId");
            String customerType = (String) BaseContextHandler.get("customerType");
            // 根据用户类型判断用户属于哪个平台
            String dev = "bigan";
            if (CustomerType.TENANT_DAO.getCode().equals(customerType)) {
                dev = "daobook";
            }

            if (currentPath == null || currentPath.isEmpty()) {
                currentPath = "/" + dev + "/" + tenantId;
            } else {
                currentPath = "/" + dev + "/" + tenantId + "/" + currentPath;
            }

            String hashDir = this.fileStat(currentPath).get("Hash").toString();
            JSONObject result = this.fileLS(hashDir);
            return ApiResult.ok(result);
        }*/
        return null;
    }
}
